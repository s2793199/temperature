package temp;

import temp.TempCalc;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class temperature extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private TempCalc tempCalc;

    public void init() throws ServletException {
        tempCalc = new TempCalc();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Temperature";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in Celsius: " +
                request.getParameter("temperature") + "\n" +
                "  <P>temperature: " +
                Double.toString(tempCalc.getTemp(request.getParameter("Celsius"))) +
                "</BODY></HTML>");
    }
}
