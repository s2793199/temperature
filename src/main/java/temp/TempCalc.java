package temp;

public class TempCalc {
    double getTemp(String temperature) {
        double fahrenheit = Integer.parseInt(temperature);
        fahrenheit = fahrenheit * 9/5 + 32;
        return fahrenheit;
    }
}
